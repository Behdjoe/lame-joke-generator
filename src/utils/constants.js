export const BASE_URL = '/'
export const ITEMS_PER_PAGE = 10

export const NAVIGATION = [
    { label: 'Home', url: '/' },
    { label: 'Search', url: '/search' },
    { label: 'Favorites', url: '/favorites', conditional: true },
    { label: 'About', url: '/about' }
]

export const JOKE_APIS = [
    {
        url: 'https://api.icndb.com/jokes/?escape=javascript',
        single: 'https://api.icndb.com/jokes/random?escape=javascript',
        key: 'value'
    },
    {
        url: 'https://icanhazdadjoke.com',
        single: 'https://icanhazdadjoke.com',
        headers: {'Accept':'application/json'}
    }
]