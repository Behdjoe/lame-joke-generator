import Vuex from 'vuex'
import {default as N} from './navigation'
import {default as J} from './jokes'
export default () => {
  return new Vuex.Store({
    modules: {
      NAV: N,
      JOKES: J
    }
  })
}