import {NAVIGATION} from '@/utils/constants'

export default {
  namespaced: true,
  state: {
    feed: NAVIGATION
  },
  getters: {
    GET_FEED(state) {
      return state.feed
    }
  }
}