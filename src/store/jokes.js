import { JOKE_APIS, ITEMS_PER_PAGE } from '@/utils/constants';

export default {
    namespaced: true,
    state: {
      feed: [],
      single: {},
      favorites: []
    },
    getters: {
      GET_FEED(state) {
        return state.feed
      },
      GET_SINGLE(state) {
        return state.single
      },
      GET_PAGES(state) {
        const combined = [state.single, ...state.feed];
        const chunkArray = (array,chunk) => array.reduce((a,b,i,g) => !(i % chunk) ? a.concat([g.slice(i,i+chunk)]) : a, []);
        return chunkArray(combined, ITEMS_PER_PAGE);
      },
      GET_FAVS(state) {
        return state.favorites
      }
    },
    mutations: {
      SET_FEED(state, payload) {
        state.feed = state.feed.concat(...payload);
      },
      SET_SINGLE(state, payload) {
          state.single = payload
      },
      SET_FAV(state, payload) {
        let isInFavs = state.favorites.map(item=> item.id).includes(payload.id)
          if (isInFavs) {
            state.favorites = state.favorites.filter(item=> item.id !== payload.id);
          } else {
            state.favorites.push(payload)
          }
      }
    },
    actions: {
        async FETCH_SINGLE({commit}) {
            JOKE_APIS.map(async item => {
                let {data} = await this.$axios.get(item.single, {headers: item.headers})
                commit('SET_SINGLE', data)
            })
        },
        async FETCH_ALL({commit, state}) {
            JOKE_APIS.map(async item => {
                let {data: {[item.key]: D=[]}} = await this.$axios.get(item.url)
                if (state.feed.length === D.length) return
                commit('SET_FEED', D)
            })
        }
    }
  }