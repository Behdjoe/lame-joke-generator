import { BASE_URL } from './src/utils/constants';

export default {
    mode: 'spa',
    srcDir: 'src',
    loading: {
        color: '#CE00BC',
        height: '2px',
    },
    env: {
        title: process.env.npm_package_name,
        description: process.env.npm_package_description,
        author: process.env.npm_package_author_name
    },
    head: {
        title: process.env.npm_package_description,
        meta: [
            { charset: 'utf-8' },
            {'http-equiv': 'X-UA-Compatible', content: 'IE=edge'},
            { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description }
        ],
        noscript: [
            {innerHTML: process.env.npm_package_description, body: true}
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/img/favicon.png' },
            { rel: 'stylesheet', type: 'text/css', href: '/fonts/fonts-all.css' }
        ]
    },
    css: ['@/assets/scss/all.scss'],
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/auth'
    ],
    auth: {
    },
    axios: {
        baseUrl: BASE_URL,
        browserBaseURL: BASE_URL,
        retry: {retries: 3}
    },
    build: {
        styleResources: {
            scss: [
                'src/assets/scss/utils/_all.scss',
                'src/assets/scss/vars/_all.scss'
            ]
        }
    },
    generate: {
        dir: 'dist',
        interval: 1000,
        subFolders: false, //flat folders
        minify: {
            removeComments: true,
            sortClassName: false
        }
    },
    cache: {
        max: 1000,
        maxAge: 900000
    }
}